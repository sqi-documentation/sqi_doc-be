package com.be.toolkit.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import com.be.toolkit.log.AuditLog;
import com.be.toolkit.models.Team;
import com.be.toolkit.responses.ResponseMessage;
import com.be.toolkit.respositories.AuditLogRepository;
import com.be.toolkit.services.TeamService;
import com.be.toolkit.utils.AuditLogUtils;

@CrossOrigin
@RestController
@RequestMapping("/api/team")
public class TeamController {

	@Autowired
	private TeamService service;
	
	@Autowired
	private AuditLogRepository logRepo;
	
	@PostMapping
	@PreAuthorize("hasRole('Admin')")
	public Team createTeam(@RequestBody Team team) {
		
		try {
			Team t = service.save(team);
			
			String entityName = AuditLogUtils.ENTITY_NAME_IS_TEAM;
			String entityKey = t.getName();
			String performedAction = AuditLogUtils.ACTION_CREATE;
			Date performedDate = t.getCreatedDate();
			String performedBy = t.getCreatedBy();
			String oldValue = null;
			String newValue = t.getDescriptionHtml();
			
			AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
			logRepo.save(log);
			
			System.out.println("[ CREATE SUCCESS ]( Team ): Create " + entityKey + "success!");
			
			return t;
			
//			return ResponseEntity
//					.status(HttpStatus.OK)
//					.body(new ResponseMessage("Successfully create team: " + entityKey + "!"));

		} catch (Exception e) {
			
			System.out.println("[ CREATE SUCCESS ]( Team ): " + e.getMessage());
			
			return null;
			
//			return ResponseEntity
//					.status(HttpStatus.OK)
//					.body(new ResponseMessage("Couldn't create team: " + e.getMessage()));
		}
	}
	
	@GetMapping
	public List<Team> findAllTeams() {
		return service.findAll();
	}	
	
	@GetMapping("/{name}")
	public Optional<Team> findTeamByName (@PathVariable("name") String name) {
		return service.findById(name);
	}
	
	@DeleteMapping("/{name}")
	@PreAuthorize("hasRole('Admin')")
	public ResponseEntity<ResponseMessage> deleteTeam(@PathVariable("name") String name) {
		
		try {
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Optional<Team> foundTeam = findTeamByName(name);
			Team t = foundTeam.get();
			
			String entityName = AuditLogUtils.ENTITY_NAME_IS_TEAM;
			String entityKey = t.getName();
			String performedAction = AuditLogUtils.ACTION_DELETE;
			Date performedDate = new Date();
			String performedBy = currentUser.getUsername();
			String oldValue = t.getDescriptionHtml();
			String newValue = null;
			
			AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
			logRepo.save(log);
			
			service.deleteById(name);
			
			return ResponseEntity
					.status(HttpStatus.OK)
					.body(new ResponseMessage("Successfully delete team: " + entityKey + "!"));
			
		} catch (Exception e) {
			return ResponseEntity
					.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ResponseMessage("Couldn't delete team: " + e.getMessage()));
		}	
	}
	
	@PutMapping("/{name}")
	@PreAuthorize("hasRole('Admin')")
	public Team updateTeam(@PathVariable("name") String name, @RequestBody Team team) {
		
		try {
			Optional<Team> foundTeam = findTeamByName(name);
			Team old_team = foundTeam.get();
			
			String entityName = AuditLogUtils.ENTITY_NAME_IS_TEAM;
			String entityKey = old_team.getName();
			String performedAction = AuditLogUtils.ACTION_UPDATE;
			String oldValue = old_team.getDescriptionHtml();
			
			old_team.setDescriptionPlainText(team.getDescriptionPlainText());
			old_team.setDescriptionBlocks(team.getDescriptionBlocks());
			old_team.setDescriptionHtml(team.getDescriptionHtml());
			Team new_team = service.save(old_team);
			
			Date performedDate = new_team.getLastModifiedDate();
			String performedBy = new_team.getLastModifiedBy();
			String newValue = new_team.getDescriptionHtml();
			
			AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
			logRepo.save(log);
			
			System.out.println("[ UPDATE SUCCESS ]( Team ): Update " + entityKey + "success!");
			
			return new_team;
			
//			return ResponseEntity
//					.status(HttpStatus.OK)
//					.body(new ResponseMessage("Successfully update team: " + entityKey + "!"));
		
		} catch (Exception e) {
			
			System.out.println("[ UPDATE SUCCESS ]( Team ): " + e.getMessage());
			
			return null;
			
//			return ResponseEntity
//					.status(HttpStatus.EXPECTATION_FAILED)
//					.body(new ResponseMessage("Couldn't update team: " + e.getMessage()));
		}
	}
}
