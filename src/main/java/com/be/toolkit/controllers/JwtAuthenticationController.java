package com.be.toolkit.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.be.toolkit.requests.JwtAuthenticationRequest;
import com.be.toolkit.responses.JwtAuthenticationResponse;
import com.be.toolkit.services.JwtService;

@CrossOrigin
@RestController
public class JwtAuthenticationController {

	@Autowired
	private JwtService jwtService;
	
	@PostMapping({"/authenticate"})
	public JwtAuthenticationResponse generateJwtToken(@RequestBody JwtAuthenticationRequest jwtAuthenticationRequest) throws Exception{
		return jwtService.createJwtToken(jwtAuthenticationRequest);
	}
}
