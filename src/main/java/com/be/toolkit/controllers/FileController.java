package com.be.toolkit.controllers;

import java.io.IOException;
import java.net.URLConnection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.be.toolkit.models.File;
import com.be.toolkit.responses.ResponseMessage;
import com.be.toolkit.services.FileStorageService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class FileController {
	
	@Autowired
	private FileStorageService service; 
	
	@PostMapping("/upload")
	@PreAuthorize("hasRole('Admin')")
	public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
		String message = "";
		try {
			service.save(file);
			message = "Successfully upload file: " + file.getOriginalFilename();
			
			return ResponseEntity.status(HttpStatus.OK)
								 .body(new ResponseMessage(message));
		} catch (Exception e) {
			message = "Couldn't upload file: " + file.getName();
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
								 .body(new ResponseMessage(message));
		}
	}
	
	@GetMapping("/files")
	public ResponseEntity<List<File>> getAllFiles() {
		List<File> files = service.loadAll().map(
			path -> {
				String filename = path.getFileName().toString();
				String url = MvcUriComponentsBuilder
								.fromMethodName(
									FileController.class,
									"downloadFile", 
									path.getFileName().toString()
								).build().toString();
				String extension = filename.split("\\.")[1];
				String filePath = "files/" + filename;
				Long byteSize = path.toFile().length(); // ?????
				String contentDisposition = "ATTACHMENT";
				String contentType = URLConnection.guessContentTypeFromName(filename);
				
				return new File(filename, url, extension, filePath, byteSize, contentDisposition, contentType);
			}
		).collect(Collectors.toList());
		
		return ResponseEntity.status(HttpStatus.OK).body(files);
	}

	@GetMapping("/files/download/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> downloadFile(@PathVariable("filename") String filename) {
		Resource file = service.load(filename);
		
		String extension = filename.split("\\.")[1];
		String contentType = "";
		if(extension.indexOf("doc") > -1) contentType = "application/msword";
		if(extension.indexOf("docx") > -1) contentType = "application/msword";
		if(extension.indexOf("xls") > -1) contentType = "application/vdn.ms-excel";
		if(extension.indexOf("csv") > -1) contentType = "application/vdn.ms-excel";
		if(extension.indexOf("ppt") > -1) contentType = "application/ppt";
		if(extension.indexOf("pdf") > -1) contentType = "application/pdf";
		if(extension.indexOf("zip") > -1) contentType = "application/zip";
		if(extension.indexOf("jpg") > -1) contentType = "image/jpeg";
		if(extension.indexOf("jpeg") > -1) contentType = "image/jpeg";
		if(extension.indexOf("png") > -1) contentType = "image/png";
		if(extension.indexOf("gif") > -1) contentType = "image/gif";
		if(extension.indexOf("eml") > -1) contentType = "message/rfc822";
				
				
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.contentType(MediaType.parseMediaType(contentType))
				.body(file);
	}
	
	@GetMapping("/files/preview/{file}")
	@ResponseBody
	public ResponseEntity<Resource> previewFile(@PathVariable File f) throws IOException {
		Resource file = service.load(f.getName());

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + file.getFilename() + "\"")
				.contentLength(file.contentLength())
				.contentType(MediaType.parseMediaType(f.getContentType()))
				.body(file);
	}
}