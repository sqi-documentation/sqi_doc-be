package com.be.toolkit.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import com.be.toolkit.log.AuditLog;
import com.be.toolkit.models.Application;
import com.be.toolkit.responses.ResponseMessage;
import com.be.toolkit.respositories.AuditLogRepository;
import com.be.toolkit.services.TeamService;
import com.be.toolkit.utils.AuditLogUtils;
import com.be.toolkit.services.ApplicationService;

import javassist.NotFoundException;

@CrossOrigin
@RestController
@RequestMapping("/api/app")
public class ApplicationController {

	@Autowired
	private ApplicationService appService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private AuditLogRepository logRepo;

	@PostMapping("/{name}")
	@PreAuthorize("hasRole('Admin')")
	public Application createApplication(@PathVariable("name") String name, @RequestBody Application application) throws NotFoundException {
			
		return teamService
				.findById(name)
				.map(
					team -> {
						try {
						
							application.setTeam(team);
							Application a = appService.save(application);
							
							String entityName = AuditLogUtils.ENTITY_NAME_IS_APP;
							String entityKey = a.getName();
							String performedAction = AuditLogUtils.ACTION_CREATE;
							Date performedDate = a.getCreatedDate();
							String performedBy = a.getCreatedBy();
							String oldValue = null;
							String newValue = a.getDescriptionHtml();
							
							AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
							logRepo.save(log);
							
							System.out.println("[ CREATE SUCCESS ]( App ): Create " + entityKey + "success!");
							
							return a;
							
						} catch (Exception e) {
							
							System.out.println("[ CREATE SUCCESS ]( App ): " + e.getMessage());
							
							return null;
						}
					}).orElseThrow(() -> new NotFoundException("Team not found with name: " + application.getName()));
	}
	
	@GetMapping("/{name}")
	public Optional<Application> findApplicationByName(@PathVariable("name") String name) {
		return appService.findById(name);
	}
	
	@GetMapping
	public List<Application> findAllApplications() {
		return appService.findAll();
	}
	
	@DeleteMapping("/{name}")
	@PreAuthorize("hasRole('Admin')")
	public ResponseEntity<ResponseMessage> deleteApplication(@PathVariable("name") String name) {
		
		try {
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Optional<Application> foundApp = findApplicationByName(name);
			Application a = foundApp.get();
			
			String entityName = AuditLogUtils.ENTITY_NAME_IS_APP;
			String entityKey = a.getName();
			String performedAction = AuditLogUtils.ACTION_DELETE;
			Date performedDate = new Date();
			String performedBy = currentUser.getUsername();
			String oldValue = a.getDescriptionHtml();
			String newValue = null;
			
			AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
			logRepo.save(log);
			
			appService.deleteById(name);
			
			return ResponseEntity
					.status(HttpStatus.OK)
					.body(new ResponseMessage("Successfully delete app: " + entityKey + "!"));
			
		} catch (Exception e) {
			return ResponseEntity
					.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ResponseMessage("Couldn't delete app: " + e.getMessage()));
		}
	}
	
	@PutMapping("/{name}")
	@PreAuthorize("hasRole('Admin')")
	public Application updateApplication(@PathVariable("name") String name, @RequestBody Application application) {
		
		try {
			Optional<Application> foundApp = findApplicationByName(name);
			Application old_app = foundApp.get();
			
			String entityName = AuditLogUtils.ENTITY_NAME_IS_APP;
			String entityKey = old_app.getName();
			String performedAction = AuditLogUtils.ACTION_UPDATE;
			String oldValue = old_app.getDescriptionHtml();
			
			old_app.setDescriptionPlainText(application.getDescriptionPlainText());
			old_app.setDescriptionBlocks(application.getDescriptionBlocks());
			old_app.setDescriptionHtml(application.getDescriptionHtml());
			Application new_app = appService.save(old_app);
			
			Date performedDate = new_app.getLastModifiedDate();
			String performedBy = new_app.getLastModifiedBy();
			String newValue = new_app.getDescriptionHtml();
			
			AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
			logRepo.save(log);
			
			System.out.println("[ UPDATE SUCCESS ]( App ): Update " + entityKey + "success!");
			
			return new_app;
			
//			return ResponseEntity
//					.status(HttpStatus.OK)
//					.body(new ResponseMessage("Successfully update application: " + entityKey + "!"));
		
		} catch (Exception e) {
			
			System.out.println("[ UPDATE SUCCESS ]( App ): " + e.getMessage());
			
			return null;
			
//			return ResponseEntity
//					.status(HttpStatus.EXPECTATION_FAILED)
//					.body(new ResponseMessage("Couldn't update application: " + e.getMessage()));
		}
	}
}
