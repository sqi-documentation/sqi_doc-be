package com.be.toolkit.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.be.toolkit.log.AuditLog;
import com.be.toolkit.respositories.AuditLogRepository;


@CrossOrigin
@RestController
@RequestMapping("/api/log")
public class AuditLogController {
	
	@Autowired
	private AuditLogRepository repo;
	
	@GetMapping("/guide")
	public List<AuditLog> getGuideLog() {
		return repo.getGuideLog("GUIDE");
	}
	
	@GetMapping("/team-app")
	public List<AuditLog> getTeamAndAppLog() {
		return repo.getTeamAndAppLog("TEAM", "APP");
	}
}
