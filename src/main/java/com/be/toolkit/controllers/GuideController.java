package com.be.toolkit.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import com.be.toolkit.log.AuditLog;
import com.be.toolkit.models.Guide;
import com.be.toolkit.responses.ResponseMessage;
import com.be.toolkit.respositories.AuditLogRepository;
import com.be.toolkit.services.GuideService;
import com.be.toolkit.utils.AuditLogUtils;


@CrossOrigin
@RestController
@RequestMapping("/api/guide")
public class GuideController {

	@Autowired
	private GuideService service;
	
	@Autowired
	private AuditLogRepository logRepo;
	
	@PostMapping
	@PreAuthorize("hasRole('Admin')")
	public Guide createGuide(@RequestBody Guide guide) {
		
		try {
			Guide g = service.saveGuide(guide);
			
			String entityName = AuditLogUtils.ENTITY_NAME_IS_GUIDE;
			String entityKey = g.getTitle();
			String performedAction = AuditLogUtils.ACTION_CREATE;
			Date performedDate = g.getCreatedDate();
			String performedBy = g.getCreatedBy();
			String oldValue = null;
			String newValue = g.getContentHtml();
			
			AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
			logRepo.save(log);
			
			System.out.println("[ CREATE SUCCESS ]( Guide ): Create " + entityKey + "success!");
			
			return g;
			
//			return ResponseEntity
//						.status(HttpStatus.OK)
//						.body(new ResponseMessage("Successfully create guide: " + entityKey + "!"));
			
		} catch (Exception e) {
			
			System.out.println("[ CREATE FAILED ]( Guide ): " + e.getMessage());
			
			return null;
			
//			return ResponseEntity
//					.status(HttpStatus.EXPECTATION_FAILED)
//					.body(new ResponseMessage("Couldn't create guide: " + e.getMessage()));
		}
	}
	
	@GetMapping
	public List<Guide> findAllGuides() {
		return service.findAll();
	}	
	
	@GetMapping("/{title}")
	public Optional<Guide> findGuideById(@PathVariable("title") String title) {
		return service.findById(title);
	}
	
	@DeleteMapping("/{title}")
	@PreAuthorize("hasRole('Admin')")
	public ResponseEntity<ResponseMessage> deleteGuide(@PathVariable("title") String title) {
		
		try {
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Optional <Guide> foundGuide = findGuideById(title);
			Guide g = foundGuide.get();
			
			String entityName = AuditLogUtils.ENTITY_NAME_IS_GUIDE;
			String entityKey = g.getTitle();
			String performedAction = AuditLogUtils.ACTION_DELETE;
			Date performedDate = new Date();
			String performedBy = currentUser.getUsername();
			String oldValue = g.getContentHtml();
			String newValue = null;
			
			AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
			logRepo.save(log);
			
			service.deleteGuideById(title);
			
			System.out.println("[ DELETE SUCCESS ]( Guide ): delete " + entityKey + "success!");
			
			return ResponseEntity
					.status(HttpStatus.OK)
					.body(new ResponseMessage("Successfully delete guide: " + entityKey + "!"));
			
		} catch (Exception e) {
			
			System.out.println("[ DELETE FAILED ]( Guide ): " + e.getMessage());
			
			return ResponseEntity
					.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ResponseMessage("Couldn't delete guide: " + e.getMessage()));
		}
	}
	
	@PutMapping("/{title}")
	@PreAuthorize("hasRole('Admin')")
	public Guide updateGuide(@PathVariable("title") String title, @RequestBody Guide guide) {
		
		try {
			Optional <Guide> foundGuide = findGuideById(title);
			Guide old_guide = foundGuide.get();
			
			String entityName = AuditLogUtils.ENTITY_NAME_IS_GUIDE;
			String entityKey = old_guide.getTitle();
			String performedAction = AuditLogUtils.ACTION_UPDATE;
			String oldValue = old_guide.getContentHtml();
			
			old_guide.setContentPlainText(guide.getContentPlainText());
			old_guide.setContentBlocks(guide.getContentBlocks());
			old_guide.setContentHtml(guide.getContentHtml());
			Guide new_guide = service.saveGuide(old_guide);
			
			Date performedDate = new_guide.getLastModifiedDate();
			String performedBy = new_guide.getLastModifiedBy();
			String newValue = new_guide.getContentHtml();
			
			AuditLog log = new AuditLog(entityName, entityKey, performedAction, performedDate, performedBy, oldValue, newValue);
			logRepo.save(log);
			
			System.out.println("[ UPDATE SUCCESS ]( Guide ): Update " + entityKey + "success!");
			
			return new_guide;
			
//			return ResponseEntity
//					.status(HttpStatus.OK)
//					.body(new ResponseMessage("Successfully update guide: " + entityKey + "!"));
		} catch (Exception e) {
			
			System.out.println("[ UPDATE FAILED ]( Guide ): " + e.getMessage());
			
			return null;
			
//			return ResponseEntity
//					.status(HttpStatus.EXPECTATION_FAILED)
//					.body(new ResponseMessage("Couldn't update guide: " + e.getMessage()));
		}
	}
}
