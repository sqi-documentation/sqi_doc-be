package com.be.toolkit.io;

public class FileInput {

	private String name;
	private String extension;
	private Long byteSize;
	private String contentType;
	private String user;
	
	public FileInput() {
	}
	
	public String getName() {
		return name;
	}
	
	public String getExtension() {
		return extension;
	}
	
	public Long getByteSize() {
		return byteSize;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public String getUser() {
		return user;
	}
}
