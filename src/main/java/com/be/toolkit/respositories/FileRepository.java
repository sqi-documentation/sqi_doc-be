package com.be.toolkit.respositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.be.toolkit.models.File;

@Repository
public interface FileRepository extends JpaRepository<File, String>{
	
}