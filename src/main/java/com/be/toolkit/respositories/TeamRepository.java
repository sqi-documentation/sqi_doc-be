package com.be.toolkit.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.be.toolkit.models.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, String>{
	
}
