package com.be.toolkit.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.be.toolkit.models.Guide;

@Repository
public interface GuideRepository extends JpaRepository<Guide, String> {
	
}
