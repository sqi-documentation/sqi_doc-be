package com.be.toolkit.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.be.toolkit.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
	
}
