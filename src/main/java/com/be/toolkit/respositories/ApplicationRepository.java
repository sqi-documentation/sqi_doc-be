package com.be.toolkit.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.be.toolkit.models.Application;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, String> {

}
