package com.be.toolkit.respositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.be.toolkit.log.AuditLog;

@Repository
public interface AuditLogRepository extends JpaRepository<AuditLog, Integer> {
	
	@Query(value = "SELECT * FROM audit_log WHERE entity_name = :entityName", nativeQuery = true)
	List<AuditLog> getGuideLog(@Param("entityName") String entityName);
	
	@Query(value = "SELECT * FROM audit_log WHERE entity_name = :entityName1 OR entity_name = :entityName2", nativeQuery = true)
	List<AuditLog> getTeamAndAppLog(@Param("entityName1") String entityName1, @Param("entityName2") String entityName2);
}
