package com.be.toolkit.respositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.be.toolkit.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
	
}
