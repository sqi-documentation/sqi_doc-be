package com.be.toolkit.models;

import javax.persistence.*;

@Entity
@Table(name = "Files")
public class File { 
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String url;
	private String extension;
	private String filePath;
	private Long byteSize;
	private String contentDisposition;
	private String contentType;
	
	public File(String name, String url, String extension, String filePath, Long byteSize, String contentDisposition, String contentType) {
		this.name = name;
		this.url = url;
		this.extension = extension;
		this.filePath = filePath;
		this.byteSize = byteSize;
		this.contentDisposition = contentDisposition;
		this.contentType = contentType;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getExtension() {
		return extension;
	}
	
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public String getFilePath() {
		return filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public Long getByteSize() {
		return byteSize;
	}

	public void setByteSize(Long byteSize) {
		this.byteSize = byteSize;
	}

	public String getContentDisposition() {
		return contentDisposition;
	}
	
	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}	
}