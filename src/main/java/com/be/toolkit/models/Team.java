package com.be.toolkit.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import com.be.toolkit.models.abstract_.AuditEntity;

@Entity
@Table(name="Teams")
public class Team extends AuditEntity<String> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(length = 50, unique = true, nullable = false)
	private String name;
	
	@Column
	private String descriptionPlainText;
	
	@Column
	private String descriptionBlocks;
	
	@Column
	private String descriptionHtml;
	
	@OneToMany(mappedBy = "team", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Application> applications;

	public Team() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescriptionPlainText() {
		return descriptionPlainText;
	}

	public void setDescriptionPlainText(String descriptionPlainText) {
		this.descriptionPlainText = descriptionPlainText;
	}

	public String getDescriptionBlocks() {
		return descriptionBlocks;
	}

	public void setDescriptionBlocks(String descriptionBlocks) {
		this.descriptionBlocks = descriptionBlocks;
	}

	public String getDescriptionHtml() {
		return descriptionHtml;
	}

	public void setDescriptionHtml(String descriptionHtml) {
		this.descriptionHtml = descriptionHtml;
	}

	public Set<Application> getApplications() {
		return applications;
	}

	public void setApplications(Set<Application> applications) {
		this.applications = applications;
	}
}
