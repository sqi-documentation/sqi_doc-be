package com.be.toolkit.models;

import java.io.Serializable;

import javax.persistence.*;

import com.be.toolkit.models.abstract_.AuditEntity;


@Entity
@Table(name="Guides")
public class Guide extends AuditEntity<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 50, unique = true, nullable = false)
	private String title;
	
	@Column
	private String contentPlainText;
	
	@Column
	private String contentBlocks;
	
	@Column
	private String contentHtml;
	
//	@OneToMany(mappedBy = "guide", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	private Set<File> files;
	
	public Guide() {
		
	}
	
	public String getTitle() {
		return title;
	}

	public String getContentPlainText() {
		return contentPlainText;
	}

	public void setContentPlainText(String contentPlainText) {
		this.contentPlainText = contentPlainText;
	}

	public String getContentBlocks() {
		return contentBlocks;
	}

	public void setContentBlocks(String contentBlocks) {
		this.contentBlocks = contentBlocks;
	}

	public String getContentHtml() {
		return contentHtml;
	}

	public void setContentHtml(String contentHtml) {
		this.contentHtml = contentHtml;
	}

//	public Set<File> getFiles() {
//		return files;
//	}
//
//	public void setFiles(Set<File> files) {
//		this.files = files;
//	}
}
