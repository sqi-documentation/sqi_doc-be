package com.be.toolkit.models;

import java.io.Serializable;

import javax.persistence.*;

import com.be.toolkit.models.abstract_.AuditEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Applications")
public class Application extends AuditEntity<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 50, unique = true, nullable = false)
	private String name;
	
	@Column
	private String descriptionPlainText;
	
	@Column
	private String descriptionBlocks;
	
	@Column
	private String descriptionHtml;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinTable(name = "team_details",
				joinColumns = {@JoinColumn(name = "team_name")},
				inverseJoinColumns = {@JoinColumn(name =  "application_name")}
	)
	@JsonIgnore
	private Team team;

	public Application() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescriptionPlainText() {
		return descriptionPlainText;
	}

	public void setDescriptionPlainText(String descriptionPlainText) {
		this.descriptionPlainText = descriptionPlainText;
	}

	public String getDescriptionBlocks() {
		return descriptionBlocks;
	}

	public void setDescriptionBlocks(String descriptionBlocks) {
		this.descriptionBlocks = descriptionBlocks;
	}

	public String getDescriptionHtml() {
		return descriptionHtml;
	}

	public void setDescriptionHtml(String descriptionHtml) {
		this.descriptionHtml = descriptionHtml;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team foundTeam) {
		this.team = foundTeam;
	}
}
