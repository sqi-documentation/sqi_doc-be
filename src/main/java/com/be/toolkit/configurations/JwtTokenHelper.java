package com.be.toolkit.configurations;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenHelper {

//	@Value("${jwt.auth.app}")
//	private String appName;
	
	@Value("${jwt.auth.secret_key}")
	private String secretKey;
	
	@Value("${jwt.auth.expires_in}")
	private Integer expiresIn;
	
	private SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;
	
	public String getUsernameFromToken(String token) {
		
		return getClaimFromToken(token, Claims::getSubject);
	}
	
	private <T> T getClaimFromToken(String token, Function<Claims, T> claimResolver) {
		Claims claims = getAllClaimsFromToken(token);	
		
		return claimResolver.apply(claims);
	}
	
	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
	}
	
	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap();
		
		return Jwts.builder()
					.setClaims(claims)
					.setSubject(userDetails.getUsername())
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + expiresIn * 5 * 1000))
					.signWith(SIGNATURE_ALGORITHM, secretKey)
					.compact();
					
	}
	
	public boolean validateToken(String token, UserDetails userDetails) {
		
		final String username = getUsernameFromToken(token);
		return(
			username != null &&
			username.equals(userDetails.getUsername()) &&
			!isTokenExpired(token)
		);
	}
	
	public boolean isTokenExpired(String token) {
		final Date expiredDate = getExpirationDate(token);
		
		return expiredDate.before(new Date());
	}
	
	private Date getExpirationDate(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}
}
