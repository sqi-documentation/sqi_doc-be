package com.be.toolkit.log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "audit_log")
public class AuditLog {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(columnDefinition = "VARCHAR(255)", insertable = false, updatable = false, nullable = false)
	private UUID id;
	
	@Column(updatable = false, nullable = false)
	// GUIDE | TEAM | APP
	private String entityName;
	
	@Column(updatable = false, nullable = false)
	private String entityKey;
	
	@Column(updatable = false, nullable = false)
	// CREATE | UPDATE | DELETE
	private String performedAction;
	
	@Column(updatable = false, nullable = false)
	// dd month yyyy hh:mm  
	private String performedDate;
	
	@Column(updatable = false, nullable = false)
	private String performedBy;
	
	private String oldValue;
	private String newValue;

	public AuditLog() {
	}
	
	public AuditLog(String entityName, String entityKey, String performedAction, Date performedDate,
			String performedBy, String oldValue, String newValue) {
		
		super();
		
		String TIMESTAMP_PATTERN = "dd MMM yyyy HH:mm"; 
		SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_PATTERN);
		String newDate = sdf.format(performedDate);
		
		this.entityName = entityName;
		this.entityKey = entityKey;
		this.performedAction = performedAction;
		this.performedDate = newDate;
		this.performedBy = performedBy;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityKey() {
		return entityKey;
	}

	public void setEntityKey(String entityKey) {
		this.entityKey = entityKey;
	}

	public String getPerformedAction() {
		return performedAction;
	}

	public void setPerformedAction(String performedAction) {
		this.performedAction = performedAction;
	}

	public String getPerformedDate() {
		return performedDate;
	}

	public void setPerformedDate(Date performedDate) {
		String TIMESTAMP_PATTERN = "dd MMM yyyy HH:mm"; 

		SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_PATTERN);
		String newDate = sdf.format(performedDate);
		
		this.performedDate = newDate;
	}

	public String getPerformedBy() {
		return performedBy;
	}

	public void setPerformedBy(String performedBy) {
		this.performedBy = performedBy;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
}
