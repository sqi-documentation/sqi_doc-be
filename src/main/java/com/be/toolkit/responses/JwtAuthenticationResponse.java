package com.be.toolkit.responses;

import com.be.toolkit.models.User;

public class JwtAuthenticationResponse {

	private User user;
	private String token;
	
	public JwtAuthenticationResponse(User user, String token) {
		this.user = user;
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
