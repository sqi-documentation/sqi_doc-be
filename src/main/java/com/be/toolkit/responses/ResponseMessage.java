package com.be.toolkit.responses;

public class ResponseMessage {

//	private String message;
	
	private final String message;
	
	public ResponseMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
