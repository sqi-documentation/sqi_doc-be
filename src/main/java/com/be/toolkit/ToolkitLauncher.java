package com.be.toolkit;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.be.toolkit.services.FileStorageService;
import com.be.toolkit.utils.AuditorAwareImpl;


@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class ToolkitLauncher {

	@Resource
	private FileStorageService service;
	
	public static void main(String[] args) {
		SpringApplication.run(ToolkitLauncher.class, args);
		System.out.println(" >>> Running ============================================");
		
		LocalDateTime date = LocalDateTime.now();
		System.out.println("     Local\t: " + date);
		String customDate1 = date.format(DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm"));
        System.out.println("     Custom\t: " + customDate1);
        System.out.println("     New\t: " + new Date());
	}
	
	@Bean
	public AuditorAware<String> auditorAware() {
		return new AuditorAwareImpl();
	}
}
