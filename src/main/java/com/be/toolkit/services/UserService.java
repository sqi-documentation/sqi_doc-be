package com.be.toolkit.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.be.toolkit.models.Role;
import com.be.toolkit.models.User;
import com.be.toolkit.respositories.RoleRepository;
import com.be.toolkit.respositories.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public User createNewUser(User user) {
		Role role = roleRepository.findById("User").get();
		
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		user.setRole(roles);
		user.setPassword(encodePassword(user.getPassword()));
		
		return userRepository.save(user);
	}
	
	public void initRolesAndUser() {
		Role adminRole = new Role();
		adminRole.setRoleName("Admin");
		adminRole.setRoleDesc("The admins are every SQI member in CTS-D who can manage all guides and documentation.");
		roleRepository.save(adminRole);
		
		Role userRole = new Role();
		userRole.setRoleName("User");
		userRole.setRoleDesc("The users are all of CTS-D member who can access all guides and documentation.");
		roleRepository.save(userRole);
		
		User adminUser = new User();
		adminUser.setUsername("U000000");
		adminUser.setEmail("asst.strg01@gmail.com");
		adminUser.setPassword(encodePassword("U000000"));
		adminUser.setActive(true);
		Set<Role> adminRoles = new HashSet<>();
		adminRoles.add(adminRole);
		adminUser.setRole(adminRoles);
		userRepository.save(adminUser);
		
		User adminUser2 = new User();
		adminUser2.setUsername("U123456");
		adminUser2.setEmail("ameliameiriska00@gmail.com");
		adminUser2.setPassword(encodePassword("U123456"));
		adminUser2.setActive(true);
		Set<Role> adminRoles2 = new HashSet<>();
		adminRoles2.add(adminRole);
		adminUser2.setRole(adminRoles2);
		userRepository.save(adminUser2);
		
		User user = new User();
		user.setUsername("U542891");
		user.setEmail("kim07.my231@gmail.com");
		user.setPassword(encodePassword("U542891"));
		user.setActive(true);
		Set<Role> userRoles = new HashSet<>();
		userRoles.add(userRole);
		user.setRole(userRoles);
		userRepository.save(user);
	}
	
	private String encodePassword(String password) {
		return passwordEncoder.encode(password);
	}
}
