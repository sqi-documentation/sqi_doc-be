package com.be.toolkit.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.be.toolkit.models.Team;
import com.be.toolkit.respositories.TeamRepository;


@Service
public class TeamService {
	
	@Autowired
	private TeamRepository repo;

	public Team save(Team team) {
		return repo.save(team);
	}
	
	public List<Team> findAll() {
		return repo.findAll();
	}
	
	public Optional<Team> findById(String name) {
		return repo.findById(name);
	}
	
	public void deleteById(String name) {
		repo.deleteById(name);
	}
}
