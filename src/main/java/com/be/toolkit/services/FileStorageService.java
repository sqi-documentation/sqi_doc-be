//package com.be.toolkit.services;
//
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.stream.Stream;
//
//import javax.annotation.PostConstruct;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//
////import javax.annotation.PostConstruct;
//
////import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.io.Resource;
//import org.springframework.core.io.UrlResource;
//import org.springframework.stereotype.Service;
//import org.springframework.util.FileSystemUtils;
//import org.springframework.web.multipart.MultipartFile;
//
//import com.be.toolkit.respositories.FileRepository;
//
//@Service
//public class FileStorageService {
//		
////	@Value("${upload.dir}")
////	private String uploadDir;
//	
//	private final Path root = Paths.get("uploads");
//	
//	private final FileRepository repo;
//	
//	@Autowired
//	public FileStorageService(FileRepository repo) {
//		this.repo = repo;
//	}
//	
//	@PostConstruct
//	public void init() {
//		try {
//			Files.createDirectories(root);
//		} catch (IOException e) {
//			throw new RuntimeException("Could not create upload folder!");
//		}
//	}
//	
//	public void save(MultipartFile mf) {
//		try {
//			if(!Files.exists(root)) {
//				init();
//			}
//			String fileName = mf.getOriginalFilename();
//			Files.copy(mf.getInputStream(), root.resolve(fileName));
//		} catch (Exception e) {
//			throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
//		}
//	}
//	
//	public Resource load(String filename) {
//	   try {
//		   Path file = root.resolve(filename);
//		   Resource resource = new UrlResource(file.toUri());
//
//		   if (resource.exists() || resource.isReadable()) {
//			   return resource;
//		   } else {
//		 	   throw new RuntimeException("Could not read the file!");
//		   }
//	   } catch (MalformedURLException e) {
//	 	   throw new RuntimeException("Error: " + e.getMessage());
//	   }
//   }
//	
////	public void delete(String filename) {
////		try {
////			Path file = root.resolve(filename);
////			Files.delete(file);
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
////	}
//	
//	public void deleteAll() {
//		FileSystemUtils.deleteRecursively(root.toFile());
//	}
//	
//	public Stream<Path> loadAll() {
//		try {
//			return Files.walk(this.root, 1)
//						.filter(path -> !path.equals(this.root))
//						.map(this.root::relativize);
//		} catch (IOException e) {
//			throw new RuntimeException("Could not load the files!");
//		}
//	}
//}

package com.be.toolkit.services;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {
  
	public void init();
	public void save(MultipartFile file);
	public Resource load(String filename);
	public void deleteAll();
	public Stream<Path> loadAll();
}