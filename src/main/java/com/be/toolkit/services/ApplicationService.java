package com.be.toolkit.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.be.toolkit.models.Application;
import com.be.toolkit.respositories.ApplicationRepository;

@Service
public class ApplicationService {

	@Autowired
	private ApplicationRepository repo;

	public Application save(Application application) {
		return repo.save(application);
	}
	
	public List<Application> findAll() {
		return repo.findAll();
	}
	
	public Optional<Application> findById(String name) {
		return repo.findById(name);
	}
	
	public void deleteById(String name) {
		repo.deleteById(name);
	}	
}
