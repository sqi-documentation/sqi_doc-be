package com.be.toolkit.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.be.toolkit.models.Guide;
import com.be.toolkit.respositories.GuideRepository;

@Service
public class GuideService {

	@Autowired
	private GuideRepository repo;

	public Guide saveGuide(Guide guide) {
		return repo.save(guide);
	}

	public List<Guide> findAll() {
		return repo.findAll();
	}
	
	public Optional<Guide> findById(String title) {
		return repo.findById(title);
	}
	
	public void deleteGuideById(String title) {
		repo.deleteById(title);
	}
}
