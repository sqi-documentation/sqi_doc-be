package com.be.toolkit.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.be.toolkit.configurations.JwtTokenHelper;
import com.be.toolkit.models.User;
import com.be.toolkit.requests.JwtAuthenticationRequest;
import com.be.toolkit.responses.JwtAuthenticationResponse;
import com.be.toolkit.respositories.UserRepository;

@Service
public class JwtService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private JwtTokenHelper jwtTokenHelper;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	public JwtAuthenticationResponse createJwtToken(JwtAuthenticationRequest jwtAuthenticationRequest) throws Exception {
		
		String username = jwtAuthenticationRequest.getUsername();
		String password = jwtAuthenticationRequest.getPassword();
		
		authenticate(username, password);
		
		final UserDetails userDetails = loadUserByUsername(username);
		String newGeneratedToken = jwtTokenHelper.generateToken(userDetails);
		User user = userRepository.findById(username).get();
		
		return new JwtAuthenticationResponse(user, newGeneratedToken);
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findById(username).get();
		
		if(user != null) {
			return new org.springframework.security.core.userdetails.User(
					user.getUsername(), 
					user.getPassword(), 
					getAuthorities(user)
			);
		} else {
			throw new UsernameNotFoundException("Username is not found!");
		}
	}
	
	private Set getAuthorities(User user) {
		Set authorities = new HashSet();
		
		user.getRole().forEach(role -> {
			authorities.add(new SimpleGrantedAuthority("ROLE_"+role.getRoleName()));
		});
		
		return authorities;
	}
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("User is disabled!");
		} catch (BadCredentialsException e) {
			throw new Exception("Bad credentials from user!");
		}
	}
}
