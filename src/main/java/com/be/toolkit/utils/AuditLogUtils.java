package com.be.toolkit.utils;

public class AuditLogUtils {

	// EntityName 
	public static final String ENTITY_NAME_IS_GUIDE = "GUIDE";
	public static final String ENTITY_NAME_IS_TEAM = "TEAM";
	public static final String ENTITY_NAME_IS_APP = "APP";
	
	// PerformedAction
	public static final String ACTION_CREATE = "CREATE";
	public static final String ACTION_UPDATE = "UPDATE";
	public static final String ACTION_DELETE = "DELETE";
}
